#! /usr/bin/python
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from stat_gen import Data_gen
import json, sys, pickle, numpy

class Bow:
	def __init__(self):
		self.vectorizer = CountVectorizer(	analyzer = "word", 	\
											tokenizer = None,	\
											preprocessor = None,\
											stop_words = None,	\
											max_features = 5000)
		self.review_list = []
		self.labels = []
		self.train_data_features = None
		self.test_reviews = []
	
	def read_to_list(self):
		elite_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_elite_reviews.txt','r')
		non_elite_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_non_elite_reviews.txt','r')
		lst_pkl = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/training.pkl','wb')
		limit = 100000
		count = 1
		for line in elite_reviews:
			if (count <= limit):
				count += 1
				self.review_list.append(line.strip())
			else:
				count = 1
				break
		for line in non_elite_reviews:
			if (count <= limit):
				count += 1
				self.review_list.append(line.strip())
			else:
				break
		pickle.dump(self.review_list, lst_pkl)
		elite_reviews.close()
		non_elite_reviews.close()
		lst_pkl.close()

	def load_training(self):
		lst_pkl = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/training.pkl','rb')
		self.review_list = pickle.load(lst_pkl)
		lst_pkl.close()

	def create_bow(self):
		self.train_data_features = self.vectorizer.fit_transform(self.review_list)
		self.train_data_features = self.train_data_features.toarray()
		vocab = self.vectorizer.get_feature_names()

	def create_labels(self):
		self.labels = numpy.ones(100000).tolist()
		self.labels = self.labels + numpy.zeros(100000).tolist()

	def random_forest(self):
		forest_pkl = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/forest.pkl','wb')
		forest = RandomForestClassifier(n_estimators = 100) 
		forest = forest.fit(self.train_data_features, self.labels)
		pickle.dump(forest,forest_pkl)
		forest_pkl.close()

	def get_baseline(self):
		clean_elite_test_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_elite_test_reviews.txt','rb')
		clean_n_elite_test_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_n_elite_test_reviews.txt','rb')
		forest_pkl = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/forest.pkl','rb')
		results_pkl = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/ne_preds.pkl','wb')
		forest = pickle.load(forest_pkl)
		dg = Data_gen()
		count = 1
		for line in clean_elite_test_reviews:
		 	self.test_reviews.append(dg.clean(line))
		for line in clean_n_elite_test_reviews:
			self.test_reviews.append(dg.clean(line))
		test_data_features = self.vectorizer.transform(self.test_reviews)
		test_data_features = test_data_features.toarray()
		result = forest.predict(test_data_features)
		pickle.dump(result,results_pkl)

		clean_elite_test_reviews.close()
		clean_n_elite_test_reviews.close()
		forest_pkl.close()
		results_pkl.close()

	def score_bow(self):
		ne_results_pkl = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/ne_preds.pkl','rb')
		e_results_pkl = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/e_preds.pkl','rb')

		e_preds = pickle.load(e_results_pkl)
		ne_preds = pickle.load(ne_results_pkl)
		e_sum = numpy.sum(e_preds)
		ne_sum = 50000 - numpy.sum(ne_preds)

		print "BoW performance: " + str((e_sum + ne_sum)/100000)


		ne_results_pkl.close()
		e_results_pkl.close()

if __name__ == "__main__":
	bow = Bow()
	#-------------------
	# bow.read_to_list()
	# bow.load_training()
	#-------------------
	# bow.create_bow()
	# bow.create_labels()
	# bow.random_forest()
	# bow.get_baseline()
	bow.score_bow()




