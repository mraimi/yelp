#! /usr/bin/python
import sys, json, codecs, csv, re
from nltk.corpus import stopwords


class Data_gen:
	def __init__(self):
		self.e_users = dict()
		self.e_reviews = dict()

	def get_elite_users(self):
		user_file = open('elite_users.json','w')
		userset = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_user.json','r')
		count = 0
		for line in userset:
			j = json.loads(line)
			if(len(j[('elite')]) > 0):
				count += 1
				self.e_users[(j[('user_id')])] = (j[('elite')],j[('review_count')])
		json.dump(self.e_users, user_file)
		print "number of elite users " + str(count)

		user_file.close()
		userset.close()

#Checks firstly if the user is Elite, then if 
#current review was written during that
#user's set of Elite years.

	def review_lookup(self, review_year, user):
		# print "review year: " + review_year
		if user not in self.e_users:
			return False
		for year in self.e_users[(user)][0]:
			if (str(review_year) == str(year)):
				# print "match"
				return True
		return False

	def build_reviews(self):
		# self.e_users = open('elite_users.json','r')
		review_txt = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_review.json','r')
		elite_reviews = open('elite_reviews.json','w')
		non_elite_reviews = open('non_elite_reviews.json','w')
		e_count = 0
		ne_count = 0
		for line in review_txt:
			r = json.loads(line)
			user = r[('user_id')]
			full_date = r[('date')]
			review_year = self.parse_year(full_date)
			review_str = r[('text')]
			self.e_reviews[(r[('user_id')])] = review_str
			if self.review_lookup(review_year, user):
				json.dump(self.e_reviews, elite_reviews)
				elite_reviews.write("\n")
				e_count += 1
			else:
				json.dump(self.e_reviews, non_elite_reviews)
				non_elite_reviews.write("\n")
				ne_count += 1
			self.e_reviews.clear()
		print "number of elite reviews: " + str(e_count)
		print "number of non-elite reviews: " + str(ne_count)

		review_txt.close()
		elite_reviews.close()
		non_elite_reviews.close()



	def dump_txt(self):
		elite_reviews = open('elite_reviews.json','r')
		non_elite_reviews = open('non_elite_reviews.json','r')
		txt_elite_reviews = codecs.open('elite_reviews.txt','w','utf-8')
		txt_non_elite_reviews = codecs.open('non_elite_reviews.txt','w','utf-8')

		count = 0
		for line in elite_reviews:
			j = json.loads(line.strip())
			review_str = ""
			for (key) in j.keys():
				review_str = j[(key)]
				txt_elite_reviews.write(review_str + "\n")
		
		for line in non_elite_reviews:
			j = json.loads(line.strip())
			review_str = ""
			for (key) in j.keys():
				review_str = j[(key)]
				txt_non_elite_reviews.write(review_str + "\n")

		elite_reviews.close()
		non_elite_reviews.close()
		txt_elite_reviews.close()
		txt_non_elite_reviews.close()

	def parse_year(self, date):
		return date[:4]

	def clean(self,string):
		string = letters_only = re.sub("[^a-zA-Z']"," ", string)
		string = string.strip().lower().split()
		words = [w for w in string if not w in stopwords.words("english")]
		return (" ".join(words))

	def pre_proc(self):
		review_txt = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_review.json','r')
		elite_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_elite_reviews.txt','w')
		non_elite_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_non_elite_reviews.txt','w')
		count = 0
		thou = 0
		for line in review_txt:
			r = json.loads(line)
			user = r[('user_id')]
			full_date = r[('date')]
			review_year = self.parse_year(full_date)
			review_str = r[('text')]
			self.e_reviews[(r[('user_id')])] = review_str
			if self.review_lookup(review_year, user):
				elite_reviews.write(self.clean(review_str) + "\n")
			else:
				non_elite_reviews.write(self.clean(review_str) + "\n")
			count += 1
			if (count == 1000):
				count = 0
				thou += 1
				print str(thou) + " x1000 written." 
			self.e_reviews.clear()
			review_txt.close()
			elite_reviews.close()
			non_elite_reviews.close()

	def create_testset(self):
		elite_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_elite_reviews.txt','r')
		non_elite_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_non_elite_reviews.txt','r')
		clean_elite_test_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_elite_test_reviews.txt','wb')
		clean_n_elite_test_reviews = open('/media/tobes/Cletus/yelp_dataset_challenge_academic_dataset/clean_n_elite_test_reviews.txt','wb')

		bypass = 1
		count = 1
		for line in elite_reviews:
			if bypass <= 100000: #bypass first 100000 reviews
				bypass +=1
				continue
			else:	
				if (count <= 50000):
					clean_elite_test_reviews.write(self.clean(line)+"\n")
					count += 1
				else:
					bypass = 1
					count = 1
					break
		for line in non_elite_reviews:
			if bypass <= 100000: #bypass first 100000 reviews
				bypass +=1
				continue
			else:		
				if count <= 50000:
					clean_n_elite_test_reviews.write(self.clean(line)+"\n")
					count += 1
				else:
					break
		
		elite_reviews.close()
		non_elite_reviews.close()
		clean_elite_test_reviews.close()
		clean_n_elite_test_reviews.close()


if __name__ == "__main__": 
	dg = Data_gen()
	# dg.get_elite_users()
	# dg.build_reviews()
	# dg.dump_csv()
	# dg.pre_proc()
	# dg.create_testset()